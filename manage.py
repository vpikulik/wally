#!/usr/bin/env python

import argparse
import sys
from os import path

sys.path.insert(0, path.normpath(path.join(
    path.dirname(path.realpath(__file__)), path.pardir)))

from core.app import create_app
from core.celery_base import celery
from core.database import db
from core.utils import drop_all_views
from core import models

from config import config


bool_parse = lambda x: x.lower() in ('1', 'true', 't')

parser = argparse.ArgumentParser()
parser.add_argument('command', type=str, nargs='+',
                    help='Command to run')
parser.add_argument('--config', required=False, default='default',
                    help='Config name')

# parser arguments
parser.add_argument('-t', '--type', type=str, nargs=1,
                    choices=('profile', 'action'), help='Object type')
parser.add_argument('-a', '--args', type=str, nargs='+', help='List of args')
parser.add_argument('-s', '--skip-first-line', type=bool_parse, nargs=1,
                    default=False, dest='skip_first_line',
                    help='Skip first line')
parser.add_argument('-l', '--header-in-first-line', type=bool_parse, nargs=1,
                    default=True, dest='header_in_first_line',
                    help='Read headers from first line')
parser.add_argument('-H', '--headers', type=str, nargs='+', help='Headers')
parser.add_argument('-d', '--csv-delimeter', type=str, nargs=1, default=';',
                    dest='csv_delimiter', help='CSV delimiter')
parser.add_argument('-f', '--file', type=str, nargs=1, help='File name')
args = parser.parse_args()


config.use(args.config)
db.start()
celery.start_celery()


if 'run' in args.command:
    app = create_app()
    app.run()

elif 'shell' in args.command:

    from werkzeug import script

    def make_shell():
        return {
            'config': config,
            'db': db,
            'models': models,
        }
    script.make_shell(make_shell, use_ipython=True)()

elif 'worker' in args.command:

    from celery.bin import worker

    worker = worker.worker(app=celery.celery)
    worker.run(loglevel='DEBUG' if config.DEBUG else 'INFO')

elif 'beat' in args.command:

    from celery.bin import beat

    beat = beat.beat(app=celery.celery)
    beat.run(loglevel='DEBUG' if config.DEBUG else 'INFO')

elif 'parser' in args.command:

    from lib.csv_parser import csv_parser, profile_runner, action_runner

    runners = {'profile': profile_runner, 'action': action_runner}
    runner = runners[args.type[0]]

    with open(args.file[0]) as file_obj:
        csv_parser(file_obj, runner, args.args,
                   header_in_first_line=args.header_in_first_line[0],
                   skip_first_line=args.skip_first_line,
                   data_field='data', headers=args.headers,
                   csv_delimiter=args.csv_delimiter)
        db.commit()

elif 'drop_db' in args.command:
    drop_all_views()
    db.finish()
    db.drop_db()

elif 'init_db' in args.command:
    db.init_db()
