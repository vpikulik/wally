
import sys
from os import path

sys.path.insert(0, path.normpath(path.join(
    path.dirname(path.realpath(__file__)), path.pardir)))

from core.app import create_app
from core import celery_base

app = create_app('config.WsgiConfig')
celery_base.celery.start_celery(app)
