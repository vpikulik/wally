
from datetime import datetime
from functools import wraps
from flask import request
from sqlalchemy.orm.exc import NoResultFound

from core.database import db
from core.models import User


def check_auth(header):
    try:
        _, api_key = header.split('=')
    except ValueError:
        return False

    try:
        user = User.query.filter(User.api_key == api_key).one()
    except NoResultFound:
        return False
    else:
        user.login_at = datetime.now()
        user.save()
        db.commit()
        return True


def get_auth_headers(headers):
    for key, value in headers:
        if key == 'Authorization':
            return value


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        header = get_auth_headers(request.headers)
        if not header or not check_auth(header):
            return {}, 401
        return f(*args, **kwargs)
    return decorated
