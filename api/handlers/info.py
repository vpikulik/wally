
from flask.ext import restful

from api.auth import requires_auth


class InfoHandler(restful.Resource):

    @requires_auth
    def get(self):
        return {'msg': 'hello'}
