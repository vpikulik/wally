
import logging
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from flask.ext import restful

from core.database import db
from core.models import Profile

logger = logging.getLogger(__name__)


class ProfileHandler(restful.Resource):

    def _profile_json(self, profile):
        return {
            'email': profile.email,
            'name': profile.name,
            'data': profile.data,
        }

    def get(self, email):
        logger.info('action="get_profile", email="%s"', email)
        try:
            profile = Profile.query.filter(Profile.email == email).one()
            return self._profile_json(profile)
        except NoResultFound:
            return None, 404

    def put(self, email):
        logger.info('action="put_profile", email="%s", %s',
                    email, request.json)
        try:
            profile = Profile.query.filter(Profile.email == email).one()
        except NoResultFound:
            profile = Profile(email=email)

        profile.name = request.json['name']
        profile.data = request.json['data']

        profile.save()
        db.commit()

        return self._profile_json(profile)
