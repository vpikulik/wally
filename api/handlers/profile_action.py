
import logging
from sqlalchemy.orm.exc import NoResultFound
from flask import request
from flask.ext import restful

from core.database import db
from core.models import Profile, ProfileAction

logger = logging.getLogger(__name__)


class ProfileActionHandler(restful.Resource):

    def _profile_event_json(self, profile_event):
        return {
            'id': profile_event.id,
            'profile': profile_event.profile.email,
            'action_type': profile_event.action_type,
            'data': profile_event.data,
        }

    def post(self, email):
        action_type = request.json.get('action_type')
        data = request.json.get('data')
        logger.info('action="post_profile_action", email="%s", '
                    'action_type="%s", %s', email, data)
        try:
            profile = Profile.query.filter(Profile.email == email).one()
        except NoResultFound:
            return None, 404

        profile_event = ProfileAction.create(
            profile=profile,
            action_type=action_type,
            data=data
        )
        db.commit()

        return self._profile_event_json(profile_event)
