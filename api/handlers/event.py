
import logging

from flask import request
from flask.ext import restful

logger = logging.getLogger(__name__)


class EventHandler(restful.Resource):

    def put(self):
        from core.tasks import receive_event

        email = request.json['email']
        event = request.json['event']
        logger.info('action="put_event", email="%s", event="%s"', email, event)
        receive_event.delay(event, email)

        return {'result': True}
