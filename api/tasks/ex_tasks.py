
from core.celery_base import celery

@celery.task
def add(x, y):
    return x + y
