
from flask.ext import restful

from api.handlers.info import InfoHandler
from api.handlers.profile import ProfileHandler
from api.handlers.profile_action import ProfileActionHandler
from api.handlers.event import EventHandler


def init_api(app):
    api = restful.Api(app)
    api.add_resource(InfoHandler, '/info/')
    api.add_resource(ProfileHandler, '/profiles/<email>/')
    api.add_resource(ProfileActionHandler, '/profiles/<email>/actions/')
    api.add_resource(EventHandler, '/events/')
