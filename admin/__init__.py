
from flask.ext.admin import Admin

from admin.views import (UserView, ProfileView, FieldView, ViewDescriptionView,
                         ReportView, EventView, TacticView, ActionView)
from core.database import db


def init_admin(app):
    admin = Admin(app)
    admin.add_view(UserView(db.session))
    admin.add_view(ProfileView(db.session))
    admin.add_view(FieldView(db.session))
    admin.add_view(ViewDescriptionView(db.session))
    admin.add_view(ReportView(db.session))
    admin.add_view(EventView(db.session))
    admin.add_view(TacticView(db.session))
    admin.add_view(ActionView(db.session))
