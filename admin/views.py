
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin.model.form import InlineFormAdmin
from flask.ext.login import current_user

from core.models import (User, Profile, Field, ViewDescription, Report, Event,
                         Tactic, Instruction, Action)


class BaseModelView(ModelView):

    edit_template = 'admin_edit.html'

    form_widget_args = {
        'created_at': {'disabled': True},
        'modified_at': {'disabled': True},
    }

    def is_accessible(self):
        return current_user.is_authenticated()


class UserView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(UserView, self).__init__(User, session, **kwargs)


class ProfileView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(ProfileView, self).__init__(Profile, session, **kwargs)


class FieldView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(FieldView, self).__init__(Field, session, **kwargs)


class ViewDescriptionView(BaseModelView):

    form_widget_args = {
        'filter': {
            'data-type': 'code',
            'data-width': '600',
            'data-height': '300',
        }
    }

    def __init__(self, session, **kwargs):
            super(ViewDescriptionView, self).__init__(
                ViewDescription, session, **kwargs)


class ReportView(BaseModelView):

    form_widget_args = {
        'query_code': {
            'data-type': 'code',
            'data-width': '600',
            'data-height': '300',
        }
    }

    def __init__(self, session, **kwargs):
            super(ReportView, self).__init__(
                Report, session, **kwargs)


class EventView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(EventView, self).__init__(Event, session, **kwargs)

    column_list = ('name', 'created_at', 'modified_at')
    form_columns = ('name',)


class InstructionInlineForm(InlineFormAdmin):

    form_columns = ('id', 'method', 'params')

    def __init__(self):
        return super(InstructionInlineForm, self).__init__(Instruction)


class TacticView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(TacticView, self).__init__(Tactic, session, **kwargs)

    column_list = ('event', 'title', 'created_at', 'modified_at')
    form_columns = ('event', 'title',)
    inline_models = (InstructionInlineForm(),)


class ActionView(BaseModelView):

    def __init__(self, session, **kwargs):
            super(ActionView, self).__init__(Action, session, **kwargs)
