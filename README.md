# Build status

[![Build Status](https://drone.io/bitbucket.org/vpikulik/wally/status.png)](https://drone.io/bitbucket.org/vpikulik/wally/latest)

# Env variables

* PROJECT
* MAIN_DOMAIN - `http://project.com/`
* DATABASE_URI - `postgres://user:password@host`
* BROKER_URL - `amqp://user:password@host:port/vhost`
* OAUTH_BACKENDS - `facebook,other`
* FACEBOOK_APP_ID
* FACEBOOK_SECRET
* LOGENTRIES_TOKEN
