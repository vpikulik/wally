
import csv

from sqlalchemy.orm.exc import NoResultFound

from core.database import db


class ParserError(Exception):
    pass


def csv_parser(file, runner, args_list, header_in_first_line=True,
               skip_first_line=False,
               data_field='data', headers=None, csv_delimiter=';'):

    reader = csv.reader(file, delimiter=csv_delimiter)

    is_headers_readed = not header_in_first_line
    is_first_line_readed = not skip_first_line

    if not header_in_first_line and not headers:
        raise ParserError()

    get_data_keys = lambda headers: headers[len(args_list):]
    get_args_keys = lambda headers: headers[:len(args_list)]
    if headers:
        args_keys = get_args_keys(headers)
        data_keys = get_data_keys(headers)

    for row in reader:

        if not row:
            continue

        if not is_first_line_readed:
            is_first_line_readed = True
            continue

        if not is_headers_readed:
            headers = row[:]
            args_keys = get_args_keys(headers)
            data_keys = get_data_keys(headers)
            is_headers_readed = True
            continue

        args_values = row[:len(args_list)]
        data_values = row[len(args_list):]

        kwargs = dict(zip(args_keys, args_values))
        kwargs[data_field] = dict(zip(data_keys, data_values))
        runner(**kwargs)


def profile_runner(**kwargs):

    from core.models import Profile

    email = kwargs['email']
    name = kwargs['name']
    data = kwargs['data']

    profile_id = Profile.exists(Profile.email == email)
    if not profile_id:
        Profile.create(email=email, name=name, data=data)
    else:
        Profile.query.filter(Profile.id == profile_id).update({
            'name': name,
            'data': data,
        })
    db.flush()


def action_runner(**kwargs):

    from core.models import Profile, ProfileAction

    email = kwargs['email']
    action_type = kwargs['action_type']
    data = kwargs['data']

    try:
        profile = Profile.query.filter(Profile.email == email).one()
    except NoResultFound:
        pass
    else:
        ProfileAction.create(
            profile=profile,
            action_type=action_type,
            data=data
        )
    db.flush()
