
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.expression import Executable, ClauseElement


class SqlViewDrop(Executable, ClauseElement):
    def __init__(self, name, materialized=False, if_exists=True):
        self.name = name
        self.materialized = materialized
        self.if_exists = if_exists


@compiles(SqlViewDrop)
def visit_sql_view_drop(element, compiler, **kw):
    return 'DROP {materialized}VIEW{if_exists} {name};'.format(
        name=element.name,
        materialized=('MATERIALIZED ' if element.materialized else ''),
        if_exists=(' IF EXISTS' if element.if_exists else ''),
    )


class SqlView(Executable, ClauseElement):
    def __init__(self, name, select, materialized=False, with_data=True):
        self.name = name
        self.select = select
        self.materialized = materialized
        self.with_data = with_data


@compiles(SqlView)
def visit_sql_view(element, compiler, **kw):
    return 'CREATE {materialized}VIEW {name} AS {select}{with_data};'.format(
        name=element.name,
        select=compiler.process(element.select),
        materialized=('MATERIALIZED ' if element.materialized else ''),
        with_data=('' if element.with_data else ' WITH NO DATA')
    )


class SqlViewRefresh(Executable, ClauseElement):
    def __init__(self, name, concurrently=False, with_data=True):
        self.name = name
        self.concurrently = concurrently
        self.with_data = with_data


@compiles(SqlViewRefresh)
def visit_sql_view_refresh(element, compiler, **kw):
    return 'REFRESH MATERIALIZED VIEW{concurrently} {name}{with_data};'.format(
        name=element.name,
        concurrently=(' CONCURRENTLY' if element.concurrently else ''),
        with_data=('' if element.with_data else ' WITH NO DATA')
    )
