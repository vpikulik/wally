
import pytest

from core.models import Profile


@pytest.mark.api_profiles
def test_profiles_put(db, client):
    status, resp = client.put(
        '/profiles/test@wl.wl/',
        params={'name': 'Test user', 'data': []}
    )

    assert status == 200
    assert resp == {'email': 'test@wl.wl', 'name': 'Test user', 'data': []}

    profile = Profile.query.filter(Profile.email == 'test@wl.wl').one()
    assert profile.name == 'Test user'
    assert profile.data == []


@pytest.mark.api_profiles
def test_profiles_put_existed(db, client):

    profile = Profile(email='pr2@wl.wl', name='Test', data=[1, 2, 3])
    profile.save()
    db.commit()

    status, resp = client.put(
        '/profiles/pr2@wl.wl/',
        params={'name': 'Updated', 'data': [7, 8, 9]}
    )

    assert status == 200
    assert resp == {'email': 'pr2@wl.wl', 'name': 'Updated', 'data': [7, 8, 9]}

    profile = Profile.query.filter(Profile.email == 'pr2@wl.wl').one()
    assert profile.name == 'Updated'
    assert profile.data == [7, 8, 9]


@pytest.mark.api_profiles
def test_profiles_get(db, client):

    profile = Profile(email='pr1@wl.wl', name='Test', data=[1, 2, 3])
    profile.save()
    db.commit()

    status, resp = client.get('/profiles/pr1@wl.wl/')
    assert status == 200
    assert resp == {'email': 'pr1@wl.wl', 'name': 'Test', 'data': [1, 2, 3]}


@pytest.mark.api_profiles
def test_profiles_get_unexisted(db, client):

    status, resp = client.get('/profiles/unexisted@wl.wl/')
    assert status == 404
