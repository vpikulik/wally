
import pytest
from mock import Mock


@pytest.fixture(scope='function')
def receive_event_mock(monkeypatch):
    receive_event = Mock()
    monkeypatch.setattr('core.tasks.receive_event', receive_event)
    return receive_event


@pytest.mark.api_event
def test_run_wait(client, receive_event_mock):
    status, resp = client.put(
        '/events/',
        params={'event': 'test', 'email': 'test@wl.wl'}
    )

    assert 200 == status
    assert {'result': True} == resp
    receive_event_mock.delay.assert_called_once_with('test', 'test@wl.wl')
