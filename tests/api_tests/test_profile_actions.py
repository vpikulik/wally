
import pytest

from core.models import Profile, ProfileAction


@pytest.fixture(scope='function')
def profile(db):
    profile = Profile(email='prof@prof.com')
    profile.save()
    db.commit()
    return profile


@pytest.mark.api_profile_actions
def test_profile_action_post_200(db, client, profile):
    status, resp = client.post(
        '/profiles/{}/actions/'.format('prof@prof.com'),
        params={'action_type': 'order', 'data': {'k1': 'v1'}}
    )
    assert status == 200
    assert resp == {'id': resp['id'], 'profile': 'prof@prof.com',
                    'action_type': 'order', 'data': {'k1': 'v1'}}

    event = ProfileAction.query.filter(ProfileAction.id == resp['id']).one()
    assert event.profile_id == profile.id
    assert event.data == {'k1': 'v1'}


@pytest.mark.api_profile_actions
def test_profile_action_post_profile_not_exist(db, client, profile):
    status, resp = client.post(
        '/profiles/{}/actions/'.format('prof@notexist.com'),
        params={'data': {'k1': 'v1'}}
    )
    assert status == 404
