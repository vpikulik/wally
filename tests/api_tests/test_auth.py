
import pytest
from datetime import datetime
from mock import Mock

from core.models import User

NOW = datetime(2014, 1, 1)


@pytest.mark.api_auth
def test_auth_without_header(client):
    status, resp = client.get(
        '/info/',
    )
    assert status == 401


@pytest.mark.api_auth
def test_auth_with_wrong_key(client):
    client.authenticate(key='678')
    status, resp = client.get(
        '/info/',
    )
    assert status == 401


@pytest.mark.api_auth
def test_auth_with_key(client, monkeypatch):

    monkeypatch.setattr('api.auth.datetime',
                        Mock(now=Mock(return_value=NOW)))

    client.authenticate()
    status, resp = client.get(
        '/info/',
    )
    assert status == 200

    user = User.query.filter(User.api_key == '123').one()
    assert user.login_at == NOW
