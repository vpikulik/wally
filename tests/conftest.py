
import pytest
import json

from core.app import create_app
from core.database import db as wdb
from core.celery_base import celery
from core.models import Event, Tactic, User
from core.utils import drop_all_views
from config import config


@pytest.fixture(scope='session', autouse=True)
def init_test_env(request):
    config.use('test')


@pytest.fixture(scope='function')
def db(request):
    """Session-wide test database."""
    wdb.start()
    wdb.init_db()

    def teardown():
        drop_all_views()
        wdb.finish()
        wdb.drop_db()
    request.addfinalizer(teardown)

    return wdb


@pytest.fixture(scope='function')
def app(request):
    """Session-wide test `Flask` application."""
    app = create_app()
    celery.start_celery()

    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


class ApiTestClient(object):

    def __init__(self, client, headers=None):
        self.client = client
        self.headers = headers or {}

    def authenticate(self, key='123'):
        self.headers['Authorization'] = 'api-key={}'.format(key)

    def _prepare_response(self, response):
        return response.status_code, json.loads(response.data)

    def get(self, url, params=None):
        resp = self.client.get(url, query_string=params, headers=self.headers)
        return self._prepare_response(resp)

    def put(self, url, params):
        resp = self.client.put(
            url,
            data=json.dumps(params),
            content_type='application/json',
            headers=self.headers)
        return self._prepare_response(resp)

    def post(self, url, params):
        resp = self.client.post(
            url,
            data=json.dumps(params),
            content_type='application/json',
            headers=self.headers)
        return self._prepare_response(resp)


@pytest.fixture(scope='function')
def client(app, db):

    user = User(api_key='123')
    user.save()
    db.commit()

    client = ApiTestClient(
        client=app.test_client()
    )
    return client


@pytest.fixture(scope='function')
def test_models(db):
    event = Event(name='test_event')
    event.save()

    tactic = Tactic(event=event, title='test_tactic')
    tactic.save()

    db.commit()

    return event, tactic


@pytest.fixture(scope='function')
def config_mock(monkeypatch):
    def patch(conf):
        from flask import current_app
        base_config = dict(current_app.config)
        base_config.update(conf)
        current_app.config = base_config
        monkeypatch.setattr('core.utils.current_app', current_app)
    return patch
