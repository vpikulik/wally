
import pytest
from mock import Mock

from core.models import Event, Tactic, Instruction


@pytest.fixture(scope='function')
def requests_mock(monkeypatch):
    request = Mock()
    monkeypatch.setattr('core.commands.requests', request)
    return request


@pytest.mark.test_intergation
def test_intergation1(db, client, requests_mock):

    # Prepare db objects

    event = Event(name='send_test')
    event.save()
    tactic = Tactic(event=event, title='testtactic')
    tactic.save()
    ins1 = Instruction(tactic=tactic, index=1, method='JS',
                       params="profile.data.v1 >= 1")
    ins1.save()
    ins2 = Instruction(
        tactic=tactic, index=2, method='JSRequest',
        params="client.post('localhost', {'email': profile.email})"
    )
    ins2.save()
    db.commit()

    # Api requests

    status, resp = client.put(
        '/profiles/user1@wl.wl/',
        params={
            'name': 'First user',
            'data': {'v1': 0}
        }
    )
    assert status == 200

    status, resp = client.put(
        '/profiles/user2@wl.wl/',
        params={
            'name': 'First user',
            'data': {'v1': 1}
        }
    )
    assert status == 200

    status, resp = client.put(
        '/events/',
        params={'event': 'send_test', 'email': 'user1@wl.wl'}
    )
    assert 200 == status

    status, resp = client.put(
        '/events/',
        params={'event': 'send_test', 'email': 'user2@wl.wl'}
    )
    assert 200 == status

    requests_mock.post.assert_called_once_with(
        'localhost',
        data={'email': 'user2@wl.wl'},
        headers={'ContentType': 'application/json'}
    )
