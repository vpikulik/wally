
import pytest
from datetime import datetime

from core.models import (ProfileDataString, ProfileDataInteger,
                         ProfileDataFloat, ProfileDataBoolean,
                         ProfileDataDatetime,
                         ActionDataString, ActionDataInteger,
                         ActionDataFloat, ActionDataBoolean,
                         ActionDataDatetime,
                         Profile, ProfileAction, Field)


@pytest.mark.profile_data
def test_clean():
    assert u'text' == ProfileDataString.clean('text')
    assert isinstance(ProfileDataString.clean('text'), unicode)

    assert 123 == ProfileDataInteger.clean('123')
    with pytest.raises(ProfileDataInteger.CleanError):
        ProfileDataInteger.clean('wrong')

    assert 89.56 == ProfileDataFloat.clean('89.56')
    with pytest.raises(ProfileDataFloat.CleanError):
        ProfileDataFloat.clean('wrong')

    assert ProfileDataBoolean.clean('False') is False
    assert ProfileDataBoolean.clean('false') is False
    assert ProfileDataBoolean.clean('F') is False
    assert ProfileDataBoolean.clean('0') is False
    assert ProfileDataBoolean.clean('True') is True
    assert ProfileDataBoolean.clean('true') is True
    assert ProfileDataBoolean.clean('T') is True
    assert ProfileDataBoolean.clean('1') is True
    with pytest.raises(ProfileDataBoolean.CleanError):
        ProfileDataBoolean.clean('wrong')

    assert datetime(2013, 11, 18, 13, 0, 0) == \
        ProfileDataDatetime.clean('2013-11-18 13:00:00')


@pytest.mark.profile_data
def test_save_profile_data(db):

    Field.create(name='str', owner='profile', ftype='string')
    Field.create(name='int', owner='profile', ftype='integer')
    Field.create(name='flt', owner='profile', ftype='float')
    Field.create(name='bool', owner='profile', ftype='boolean')
    Field.create(name='date', owner='profile', ftype='datetime')

    db.commit()

    profile = Profile.create(
        name='Profile',
        email='email@email.em',
        data={
            'str': 'text',
            'int': '3689',
            'flt': '123.45',
            'bool': 'true',
            'date': '2014-10-11 23:58',
        }
    )
    db.commit()

    assert 'text' == ProfileDataString.query.filter(
        (ProfileDataString.owner == profile) &
        (ProfileDataString.field_id == Field.id) &
        (Field.name == 'str')
    ).one().data
    assert 3689 == ProfileDataInteger.query.filter(
        (ProfileDataInteger.owner == profile) &
        (ProfileDataInteger.field_id == Field.id) &
        (Field.name == 'int')
    ).one().data
    assert 123.45 == ProfileDataFloat.query.filter(
        (ProfileDataFloat.owner == profile) &
        (ProfileDataFloat.field_id == Field.id) &
        (Field.name == 'flt')
    ).one().data
    assert ProfileDataBoolean.query.filter(
        (ProfileDataBoolean.owner == profile) &
        (ProfileDataBoolean.field_id == Field.id) &
        (Field.name == 'bool')
    ).one().data is True
    assert datetime(2014, 10, 11, 23, 58) == ProfileDataDatetime.query.filter(
        (ProfileDataDatetime.owner == profile) &
        (ProfileDataDatetime.field_id == Field.id) &
        (Field.name == 'date')
    ).one().data

    # save second time
    profile.data = {
        'str': 'text_new',
        'int': '36891',
        'flt': '567.65',
        'bool': 'f',
        'date': '2014-10-12 22:56',
    }
    profile.save()
    db.commit()

    assert 'text_new' == ProfileDataString.query.filter(
        (ProfileDataString.owner == profile) &
        (ProfileDataString.field_id == Field.id) &
        (Field.name == 'str')
    ).one().data
    assert 36891 == ProfileDataInteger.query.filter(
        (ProfileDataInteger.owner == profile) &
        (ProfileDataInteger.field_id == Field.id) &
        (Field.name == 'int')
    ).one().data
    assert 567.65 == ProfileDataFloat.query.filter(
        (ProfileDataFloat.owner == profile) &
        (ProfileDataFloat.field_id == Field.id) &
        (Field.name == 'flt')
    ).one().data
    assert ProfileDataBoolean.query.filter(
        (ProfileDataBoolean.owner == profile) &
        (ProfileDataBoolean.field_id == Field.id) &
        (Field.name == 'bool')
    ).one().data is False
    assert datetime(2014, 10, 12, 22, 56) == ProfileDataDatetime.query.filter(
        (ProfileDataDatetime.owner == profile) &
        (ProfileDataDatetime.field_id == Field.id) &
        (Field.name == 'date')
    ).one().data


@pytest.mark.profile_data
def test_save_profile_action_data(db):

    Field.create(name='str', owner='action', ftype='string')
    Field.create(name='int', owner='action', ftype='integer')
    Field.create(name='flt', owner='action', ftype='float')
    Field.create(name='bool', owner='action', ftype='boolean')
    Field.create(name='date', owner='action', ftype='datetime')
    db.commit()

    profile = Profile.create(
        name='Profile',
        email='email@email.em',
    )

    ProfileAction.create(
        profile=profile,
        action_type='payment',
        data={
            'str': 'actiontext',
            'int': '8765',
            'flt': '22.12',
            'bool': 'false',
            'date': '2014-12-10 21:47',
        }
    )
    db.commit()

    assert 'actiontext' == ActionDataString.query.filter(
        (ActionDataString.owner == profile) &
        (ActionDataString.field_id == Field.id) &
        (Field.name == 'str')
    ).one().data
    assert 8765 == ActionDataInteger.query.filter(
        (ActionDataInteger.owner == profile) &
        (ActionDataInteger.field_id == Field.id) &
        (Field.name == 'int')
    ).one().data
    assert 22.12 == ActionDataFloat.query.filter(
        (ActionDataFloat.owner == profile) &
        (ActionDataFloat.field_id == Field.id) &
        (Field.name == 'flt')
    ).one().data
    assert ActionDataBoolean.query.filter(
        (ActionDataBoolean.owner == profile) &
        (ActionDataBoolean.field_id == Field.id) &
        (Field.name == 'bool')
    ).one().data is False
    assert datetime(2014, 12, 10, 21, 47) == ActionDataDatetime.query.filter(
        (ActionDataDatetime.owner == profile) &
        (ActionDataDatetime.field_id == Field.id) &
        (Field.name == 'date')
    ).one().data
