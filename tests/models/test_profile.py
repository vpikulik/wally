
import pytest

from core.models import Profile


@pytest.mark.profile
def test_get_context():
    profile = Profile(
        email='em@wl.wl',
        name='Proname',
        data={'v1': 1, 'v2': 2}
    )
    assert profile.get_context() == {
        'email': 'em@wl.wl',
        'name': 'Proname',
        'data': {'v1': 1, 'v2': 2},
    }
