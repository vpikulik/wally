
import pytest
from mock import Mock

from core.models import Instruction


@pytest.fixture(scope='function')
def mock_command():
    return Mock()


@pytest.fixture(scope='function')
def command_library(monkeypatch, mock_command):
    monkeypatch.setattr('core.commands.library', {'WAIT': mock_command})


@pytest.fixture(scope='function')
def instruction():
    return Instruction(
        method='WAIT',
        compiled_params='params'
    )


def test_load_command(command_library, mock_command, instruction):
    assert mock_command.return_value == instruction.load_command()
    mock_command.assert_called_once_with()


def test_run_command(command_library, instruction):
    mock_command = Mock()
    instruction.load_command = Mock(return_value=mock_command)

    action = Mock()
    result = instruction.run(action)

    assert result == mock_command.run.return_value
    mock_command.run.assert_called_once_with(action=action)


def test_save_instruction(db, monkeypatch, instruction):
    mock_command = Mock()
    mock_command.parse = Mock(return_value={'a': 'b'})

    instruction.params = '50 seconds'
    instruction.load_command = Mock(return_value=mock_command)
    instruction.save()

    mock_command.parse.assert_called_once_with('50 seconds')
    assert instruction.compiled_params == {'a': 'b'}
