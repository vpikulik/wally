
import pytest
from datetime import datetime
from mock import Mock, patch

from core.models import Instruction, Action, Profile


NOW = datetime(2014, 12, 12, 0, 0)


@pytest.fixture(scope='function')
def instruction(test_models):
    event, tactic = test_models
    instr = Instruction(tactic=tactic)
    instr.load_command = Mock()
    return instr


@pytest.mark.action_run
@patch('core.models.strategy.datetime', Mock(now=lambda: NOW))
def test_NEW_action_returned(instruction):

    mock_command = Mock()
    mock_new_action = Action(start_time=NOW)
    mock_new_action.save = Mock()
    mock_new_action.run = Mock()
    mock_command.run.return_value = mock_new_action

    instruction.load_command.return_value = mock_command

    action = Action(instruction=instruction)
    action.run()

    assert False == mock_new_action.save.called
    mock_new_action.run.assert_called_once_with()


@pytest.mark.action_run
@patch('core.models.strategy.datetime', Mock(now=lambda: NOW))
def test_NEW_action_returned_with_time_for_delay(instruction):

    mock_command = Mock()
    mock_new_action = Action(start_time=datetime(2015, 1, 1))
    mock_new_action.save = Mock()
    mock_new_action.run = Mock()
    mock_command.run.return_value = mock_new_action

    instruction.load_command.return_value = mock_command

    action = Action(instruction=instruction)
    action.run()

    assert True == mock_new_action.save.called
    assert False == mock_new_action.run.called


@pytest.mark.action_run
def test_NONE_was_returned(instruction):

    mock_command = Mock()
    mock_command.run.return_value = None

    mock_new_action = Mock()
    with patch(
        'core.models.Action.create',
        Mock(return_value=mock_new_action)
    ) as mock_create:

        Action.create = mock_create

        instruction.load_command.return_value = mock_command

        action = Action(instruction=instruction)
        action.profile = Mock()
        action.run()

        mock_create.assert_called_once_with(
            tactic=instruction.tactic,
            profile=action.profile,
            previous_action=action
        )
        assert True == mock_new_action.process.called


@pytest.mark.action_run
def test_sucess_CANCEL_was_returned(instruction):

    mock_command = Mock()
    mock_command.run.side_effect = Instruction.CancelWithSuccess

    instruction.load_command.return_value = mock_command

    action = Action(instruction=instruction)
    action.run()

    # no errors and exceptions expected


@pytest.mark.action_run
def test_error_CANCEL_was_returned(instruction):

    mock_command = Mock()
    mock_command.run.side_effect = Instruction.CancelWithError

    instruction.load_command.return_value = mock_command

    action = Action(instruction=instruction)
    action.run()

    # no errors and exceptions expected
    # TODO: check logs here


# TODO: move to conftest
@pytest.fixture(scope='function')
def test_tactic(db, test_models):

    event, tactic = test_models

    inst1 = Instruction(tactic=tactic, index=1, method='FAKE')
    inst1.save()
    inst2 = Instruction(tactic=tactic, index=2, method='FAKE')
    inst2.save()
    inst3 = Instruction(tactic=tactic, index=3, method='FAKE')
    inst3.save()
    db.commit()

    return tactic, (inst1, inst2, inst3)


@pytest.mark.action_create
def test_action_create(test_tactic, db):

    tactic, (inst1, inst2, inst3) = test_tactic
    profile = Profile(email='em@web.by')
    profile.save()
    db.commit()

    action = Action.create(tactic, profile)
    assert profile == action.profile
    assert inst1 == action.instruction


@pytest.mark.action_create
def test_action_create_from_previous(test_tactic, db):

    tactic, (inst1, inst2, inst3) = test_tactic
    profile = Profile(email='em@web.by')
    profile.save()
    db.commit()

    action = Action.create(tactic, profile, previous_instruction=inst1)
    assert profile == action.profile
    assert inst2 == action.instruction


@pytest.mark.action_create
def test_action_create_from_previous_with_last_instr(test_tactic, db):

    tactic, (inst1, inst2, inst3) = test_tactic
    profile = Profile(email='em@web.by')
    profile.save()
    db.commit()

    action = Action.create(tactic, profile, previous_instruction=inst3)
    assert None == action
