
import pytest

from core.models import Instruction


@pytest.fixture(scope='function')
def test_tactic(db, test_models):

    event, tactic = test_models

    inst1 = Instruction(tactic=tactic, index=1, method='FAKE')
    inst1.save()
    inst2 = Instruction(tactic=tactic, index=2, method='FAKE')
    inst2.save()
    inst3 = Instruction(tactic=tactic, index=3, method='FAKE')
    inst3.save()
    db.commit()

    return tactic, (inst1, inst2, inst3)


def test_get_instruction(test_tactic):
    tactic, (inst1, inst2, inst3) = test_tactic
    assert inst1 == tactic.get_instruction()


def test_get_instruction_with_previous(test_tactic):
    tactic, (inst1, inst2, inst3) = test_tactic
    assert inst3 == tactic.get_instruction(previous=inst2)


def test_get_instruction_previous_is_last(test_tactic):
    tactic, (inst1, inst2, inst3) = test_tactic
    assert None == tactic.get_instruction(previous=inst3)
