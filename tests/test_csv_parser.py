
import pytest
from mock import Mock, call
from StringIO import StringIO

from lib.csv_parser import csv_parser, ParserError


CSV_CONTENT = """
email;field1;field2;field3
test1@em.de;foo1;foo2;foo3
test2@em.de;bar1;bar2;bar3
"""


@pytest.fixture(scope='function')
def csv1(request):
    fl = StringIO(CSV_CONTENT)

    def teardown():
        fl.close()
    request.addfinalizer(teardown)
    return fl


def test_csv_parser_load_header_from_file(csv1):

    runner_mock = Mock()
    assert runner_mock.call_count == 0

    csv_parser(file=csv1, args_list=('email',), runner=runner_mock,
               data_field='data_field')

    assert runner_mock.call_args_list == [
        call(
            email='test1@em.de',
            data_field={'field1': 'foo1', 'field2': 'foo2', 'field3': 'foo3'}
        ),
        call(
            email='test2@em.de',
        data_field={'field1': 'bar1', 'field2': 'bar2', 'field3': 'bar3'}
        ),
    ]


def test_csv_parser_skip_first_line_no_headers(csv1):

    runner_mock = Mock()

    with pytest.raises(ParserError):
        csv_parser(file=csv1, runner=runner_mock, skip_first_line=True,
                   header_in_first_line=False, args_list=('email',),
                   data_field='jfield', headers=None)


def test_csv_parser_skip_first_line_with_headers(csv1):

    runner_mock = Mock()
    assert runner_mock.call_count == 0

    csv_parser(file=csv1, runner=runner_mock, skip_first_line=True,
               header_in_first_line=False, args_list=('email',),
               data_field='jfield', headers=('em', 'f1', 'f2', 'f3'))

    assert runner_mock.call_args_list == [
        call(
            em='test1@em.de',
            jfield={'f1': 'foo1', 'f2': 'foo2', 'f3': 'foo3'}
        ),
        call(
            em='test2@em.de',
            jfield={'f1': 'bar1', 'f2': 'bar2', 'f3': 'bar3'}
        ),
    ]
