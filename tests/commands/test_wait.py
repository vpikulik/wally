
import pytest
from datetime import datetime, timedelta
from mock import patch, Mock

from core.commands import InvalidCommand, WaitCommand
from core.models import Instruction, Action, Profile

NOW = datetime(2014, 10, 8)


@pytest.fixture(scope='function')
def command():
    return WaitCommand()


@pytest.mark.command_wait
def test_parse_without_space(command):
    assert command.parse('60seconds') == {'timeout': 60}


@pytest.mark.command_wait
def test_wrong_nominative(command):
    with pytest.raises(InvalidCommand):
        command.parse('60things')


@pytest.mark.command_wait
def test_wrong_value(command):
    with pytest.raises(InvalidCommand):
        command.parse('ABthings')


@pytest.mark.command_wait
def test_parse_seconds(command):
    assert command.parse('50 seconds') == {'timeout': 50}
    assert command.parse('2 second') == {'timeout': 2}


@pytest.mark.command_wait
def test_parse_minutes(command):
    assert command.parse('50 minutes') == {'timeout': 3000}
    assert command.parse('2 minute') == {'timeout': 120}


@pytest.mark.command_wait
def test_parse_hours(command):
    assert command.parse('20 hours') == {'timeout': 72000}
    assert command.parse('1 hour') == {'timeout': 3600}


@pytest.mark.command_wait
def test_parse_days(command):
    assert command.parse('10 days') == {'timeout': 864000}
    assert command.parse('1 day') == {'timeout': 86400}


@pytest.mark.command_wait
@patch('core.commands.datetime', Mock(now=lambda: NOW))
def test_run_wait(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='WAIT',
                               params='100 seconds')
    instruction1.save()
    instruction2 = Instruction(tactic=tactic, index=2, method='FAKE')
    instruction2.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    result = command.run(action1)

    assert isinstance(result, Action)
    assert result.instruction == instruction2
    assert result.profile == profile
    assert result.start_time == NOW + timedelta(seconds=100)


@pytest.mark.command_wait
def test_run_wait_last_action(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='WAIT',
                               params='100 seconds')
    instruction1.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    result = command.run(action1)

    assert result is None
