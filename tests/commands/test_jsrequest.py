
import pytest
from mock import Mock, patch

from core.commands import JsRequestCommand


@pytest.fixture(scope='function')
def command():
    return JsRequestCommand()


@pytest.fixture(scope='function')
def requests_mock(monkeypatch):
    response = Mock(status_code=200)
    mock = Mock(
        get=Mock(return_value=response),
        post=Mock(return_value=response),
        put=Mock(return_value=response),
        delete=Mock(return_value=response),
    )
    monkeypatch.setattr('core.commands.requests', mock)
    return mock


@pytest.fixture(scope='function')
def prepare_response_mock(monkeypatch):
    mock = Mock()
    monkeypatch.setattr('core.commands.JsRequestCommand.prepare_response',
                        mock)
    return mock


@pytest.fixture(scope='function')
def headers_mock(monkeypatch):
    mock = lambda self, x: x
    monkeypatch.setattr('core.commands.JsRequestCommand.headers', mock)
    return mock


@pytest.mark.command_jsrequest
def test_request_get(requests_mock, prepare_response_mock, headers_mock,
                     command):
    resp = command.get('url', {'a': '1'}, {'h1': 'abc'})
    assert resp == prepare_response_mock.return_value
    requests_mock.get.assert_called_once_with(
        'url',
        params={'a': '1'},
        headers={'h1': 'abc'}
    )


@pytest.mark.command_jsrequest
def test_request_post(requests_mock, prepare_response_mock, headers_mock,
                      command):
    resp = command.post('url', {'a': '1'}, {'h1': 'abc'})
    assert resp == prepare_response_mock.return_value
    requests_mock.post.assert_called_once_with(
        'url',
        data={'a': '1'},
        headers={'h1': 'abc'}
    )


@pytest.mark.command_jsrequest
def test_request_put(requests_mock, prepare_response_mock, headers_mock,
                     command):
    resp = command.put('url', {'a': '1'}, {'h1': 'abc'})
    assert resp == prepare_response_mock.return_value
    requests_mock.put.assert_called_once_with(
        'url',
        data={'a': '1'},
        headers={'h1': 'abc'}
    )


@pytest.mark.command_jsrequest
def test_request_delete(requests_mock, prepare_response_mock, headers_mock,
                        command):
    resp = command.delete('url', {'h1': 'abc'})
    assert resp == prepare_response_mock.return_value
    requests_mock.delete.assert_called_once_with(
        'url',
        headers={'h1': 'abc'}
    )


@pytest.mark.command_jsrequest
def test_request_get_run(command):
    with patch('core.commands.JsRequestCommand.get') as get_mock:
        context = command.get_js_context()
        context.execute('client.get("url1", {"a": 1}, {"h0": "h1"})')
        get_mock.assert_called_once_with('url1', {'a': 1}, {'h0': 'h1'})


@pytest.mark.command_jsrequest
def test_request_post_run(command):
    with patch('core.commands.JsRequestCommand.post') as get_mock:
        context = command.get_js_context()
        context.execute('client.post("url2", {"a": 1}, {"h0": "h1"})')
        get_mock.assert_called_once_with('url2', {'a': 1}, {'h0': 'h1'})


@pytest.mark.command_jsrequest
def test_request_put_run(command):
    with patch('core.commands.JsRequestCommand.put') as get_mock:
        context = command.get_js_context()
        context.execute('client.put("url3", {"a": 1}, {"h0": "h1"})')
        get_mock.assert_called_once_with('url3', {'a': 1}, {'h0': 'h1'})


@pytest.mark.command_jsrequest
def test_request_delete_run(command):
    with patch('core.commands.JsRequestCommand.delete') as get_mock:
        context = command.get_js_context()
        context.execute('client.delete("url4", {"h0": "h1"})')
        get_mock.assert_called_once_with('url4', {'h0': 'h1'})


@pytest.mark.command_jsrequest
def test_request_prepare_bad_answer(command):
    resp = command.prepare_response(Mock(status_code=401))
    assert resp is False


@pytest.mark.command_jsrequest
def test_request_prepare_success(command):
    answer = Mock(status_code=200)
    resp = command.prepare_response(answer)
    assert resp == answer.json.return_value


@pytest.mark.command_jsrequest
def test_request_prepare_not_json(command):
    json_mock = Mock(side_effect=ValueError)
    answer = Mock(status_code=200, json=json_mock)
    resp = command.prepare_response(answer)
    assert resp == answer.content
