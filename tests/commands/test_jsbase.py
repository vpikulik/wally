
import pytest

from core.commands import JsBaseCommand
from core.models import Instruction, Action, Profile


@pytest.fixture(scope='function')
def command():
    return JsBaseCommand()


@pytest.mark.command_jsbase
def test_empty_params(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='JS', params='')
    instruction1.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    with pytest.raises(Instruction.CancelWithError):
        command.run(action1)


@pytest.mark.command_jsbase
def test_js_error(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='JS',
                               params='1abc')
    instruction1.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    with pytest.raises(Instruction.CancelWithError):
        command.run(action1)


@pytest.mark.command_jsbase
def test_run_positive_decision(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='JS',
                               params='true')
    instruction1.save()
    instruction2 = Instruction(tactic=tactic, index=2, method='FAKE')
    instruction2.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    result = command.run(action1)

    assert isinstance(result, Action)
    assert result.instruction == instruction2
    assert result.profile == profile


@pytest.mark.command_jsbase
def test_run_negative_decision(command, db, test_models):

    event, tactic = test_models
    instruction1 = Instruction(tactic=tactic, index=1, method='JS',
                               params='false')
    instruction1.save()
    instruction2 = Instruction(tactic=tactic, index=2, method='FAKE')
    instruction2.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    result = command.run(action1)

    assert result is None


@pytest.mark.command_jsbase
def test_save_profile(command, db, test_models):
    event, tactic = test_models
    params = """
    profile.name = 'Newusername';
    profile.data.v1 = 1;
    profile.data.v2 = 2;
    profile.save();
    """
    instruction1 = Instruction(tactic=tactic, index=1, method='JS',
                               params=params)
    instruction1.save()
    profile = Profile(
        email='user@wl.com',
        name='Username',
        data={
            'v1': 0,
        }
    )
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    command.run(action1)

    db_profile = Profile.query.filter(Profile.email == 'user@wl.com').one()

    assert 'Newusername' == db_profile.name
    assert {'v1': 1, 'v2': 2} == db_profile.data


@pytest.mark.command_jsbase
def test_save_cannot_change_profile_email(command, db, test_models):
    event, tactic = test_models
    params = """
    profile.email = 'other@em.em';
    profile.save();
    """
    instruction1 = Instruction(tactic=tactic, index=1, method='JS',
                               params=params)
    instruction1.save()
    profile = Profile(email='user@wl.com')
    profile.save()
    action1 = Action(instruction=instruction1, profile=profile)
    db.commit()

    with pytest.raises(Instruction.CancelWithError):
        command.run(action1)

    assert 1 == Profile.query.filter(Profile.email == 'user@wl.com').count()
    assert 0 == Profile.query.filter(Profile.email == 'other@em.em').count()
