
# import pytest
from mock import Mock, call, patch

from core.models import Profile, Tactic


def test_receive_event(db, test_models):

    from core.tasks import receive_event

    event, tactic1 = test_models

    tactic2 = Tactic(event=event)
    tactic2.save()

    profile = Profile(email='email@wl.by')
    profile.save()

    db.commit()

    mock_created_action = Mock()
    mock_create = Mock(return_value=mock_created_action)

    with patch('core.models.Action.create', mock_create):
        receive_event('test_event', 'email@wl.by')

    assert mock_create.call_args_list == [
        call(
            tactic=tactic1,
            profile=profile,
        ),
        call(
            tactic=tactic2,
            profile=profile,
        ),
    ]
    assert 2 == mock_created_action.run.call_count


def test_receive_event_with_unexisted_profile(db, test_models):

    from core.tasks import receive_event

    event, tactic1 = test_models

    mock_create = Mock()

    with patch('core.models.Action.create', mock_create):
        receive_event('test_event', 'email@wl.by')

    assert False == mock_create.called


def test_receive_event_with_unexisted_event(db, test_models):

    from core.tasks import receive_event

    event, tactic1 = test_models

    profile = Profile(email='email@wl.by')
    profile.save()

    db.commit()

    mock_create = Mock()

    with patch('core.models.Action.create', mock_create):
        receive_event('unexisted_event', 'email@wl.by')

    assert False == mock_create.called
