
from datetime import datetime, timedelta
from mock import patch, Mock


from core.models import Profile, Instruction, Action

NOW = datetime(2012, 7, 14)


@patch('core.tasks.datetime', Mock(now=Mock(return_value=NOW)))
def test_look_for_actions(db, test_models):
    from core.tasks import look_for_actions

    event, tactic = test_models

    instruction = Instruction(tactic=tactic, index=1, method='FAKE')
    instruction.save()

    profile = Profile(email='pr@web.gt')
    profile.save()

    action = Action(instruction=instruction, profile=profile,
                    start_time=NOW + timedelta(seconds=1000))
    action.save()
    active_action1 = Action(instruction=instruction, profile=profile,
                            start_time=NOW - timedelta(seconds=1000))
    active_action1.save()
    active_action2 = Action(instruction=instruction, profile=profile,
                            start_time=None)
    active_action2.save()

    runs = set()
    Action.run = lambda self: runs.add(self.id)

    db.commit()

    look_for_actions()

    assert runs == set((active_action1.id, active_action2.id))
