
import pytest
from datetime import datetime
from mock import Mock

from core.models import User
from core.auth import get_user, AuthError, login_user


NOW = datetime(2012, 01, 12)


@pytest.mark.auth_get_user
def test_get_user_project_name(config_mock, db, app):
    config_mock({'PROJECT': 'test_project'})

    # create user
    user = User(email='test@tst.com')
    user.save()
    db.commit()

    # project with right name
    result = get_user(email='test@tst.com', projects=['test_project'])
    assert result.id == user.id

    # all projects
    result = get_user(email='test@tst.com', projects=['*'])
    assert result.id == user.id

    # wrong project
    with pytest.raises(AuthError):
        get_user(email='test@tst.com', projects=['wrong'])


@pytest.mark.auth_get_user
def test_get_user_not_allow_to_create(config_mock, app, db):
    config_mock({'PROJECT': 'test_project', 'OAUTH_USER_CREATION': False})

    with pytest.raises(AuthError):
        get_user(email='test@tst.com', projects=['test_project'])


@pytest.mark.auth_get_user
def test_get_existed_profile(config_mock, app, db):
    config_mock({'PROJECT': 'test_project', 'OAUTH_USER_CREATION': False})

    # create user
    user = User(email='test@tst.com')
    user.save()
    db.commit()

    result = get_user(email='test@tst.com', projects=['test_project'])
    assert result.id == user.id


@pytest.mark.auth_get_user
def test_get_user_when_does_not_exist(config_mock, app, db):
    config_mock({'PROJECT': 'test_project', 'OAUTH_USER_CREATION': True})

    result = get_user(email='new@tst.com', projects=['test_project'],
                      name='Username', role='admin')
    assert isinstance(result, User)
    assert bool(result.id)
    assert result.email == 'new@tst.com'
    assert result.name == 'Username'
    assert result.role == 'admin'


@pytest.mark.auth_get_user
def test_login_user(monkeypatch):
    monkeypatch.setattr('core.auth.datetime',
                        Mock(now=Mock(return_value=NOW)))
    flask_login_mock = Mock()
    monkeypatch.setattr('core.auth.flask_login_user', flask_login_mock)

    user = Mock()
    login_user(user)
    assert user.login_at == NOW
    flask_login_mock.assert_called_once_with(user)
