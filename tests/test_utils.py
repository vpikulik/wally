
import pytest
import spidermonkey
from mock import Mock

from core.utils import spidermonkey_load, get_config, url


@pytest.mark.test_utils
def test_spidermonkey_load():
    rt = spidermonkey.Runtime()
    cx = rt.new_context()

    obj = cx.execute("var a = {b: [1,2,3,4,5]}; a")
    assert isinstance(obj, spidermonkey.Object)
    assert {'b': [1, 2, 3, 4, 5]} == spidermonkey_load(obj)

    obj = cx.execute("var a = [{a: 1, b: 2}, {c: 3, d: 4}]; a")
    assert isinstance(obj, spidermonkey.Object)
    assert [{'a': 1, 'b': 2}, {'c': 3, 'd': 4}] == spidermonkey_load(obj)


@pytest.mark.test_utils
def test_get_config(app, monkeypatch):
    monkeypatch.setattr('flask.current_app.config', {'Key': 'Value'})

    assert get_config('Key') == 'Value'
    assert get_config('does_not_exist', 'default') == 'default'
    assert get_config('does_not_exist') is None


@pytest.mark.test_utils
def test_url(config_mock, app, monkeypatch):
    config_mock({'MAIN_DOMAIN': 'http://host/'})
    url_for_mock = Mock(return_value='/path/')
    monkeypatch.setattr('core.utils.url_for', url_for_mock)

    assert 'http://host/path/' == url('path', absolute=True, a=1, b=1)
    url_for_mock.assert_called_with('path', a=1, b=1)
    assert '/path/' == url('path', absolute=False, a=2, b=2)
    url_for_mock.assert_called_with('path', a=2, b=2)
    assert '/path/' == url('path', a=3, b=3)
    url_for_mock.assert_called_with('path', a=3, b=3)
