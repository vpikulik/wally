
from celery.schedules import crontab


class DefaultConfig(object):
    DEBUG = True
    TESTING = False

    PROJECT = 'wally'
    MAIN_DOMAIN = 'http://localhost:5000/'

    DATABASE_URI = 'postgresql://wally:wally@localhost/wally'

    BROKER_URL = 'amqp://guest:guest@localhost:5672//'
    CELERY_ALWAYS_EAGER = False
    CELERY_IMPORTS = (
        'core.tasks',
    )
    CELERYBEAT_SCHEDULE = {
        'update_view': {
            'task': 'core.tasks.update_view',
            'schedule': crontab(hour='0,8,10,12,14,16,18,20', minute=0),
        },
    }

    OAUTH_BACKENDS = ['facebook']
    OAUTH_USER_CREATION = True

    FACEBOOK_APP_ID = '366584113507472'
    FACEBOOK_SECRET = '69c43e886098ca2712a9aa7002ce055b'

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
            },
            'file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': '/tmp/wally.log',
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console', 'file'],
                'level': 'INFO',
                'propagate': True,
            },
        }
    }
