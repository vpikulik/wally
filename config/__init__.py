
import os

from config.default import DefaultConfig
from config.test import TestConfig
from config.wsgi import WsgiConfig

# try:
#     from config.local import DefaultConfig
# except ImportError:
#     pass
# try:
#     from config.local import TestConfig
# except ImportError:
#     pass


class Config(object):

    CONFIGS = {
        'default': DefaultConfig(),
        'test': TestConfig(),
        'wsgi': WsgiConfig(),
    }

    def __init__(self):
        self.current = 'default'

        conf = os.environ.get('CONFIG')
        if conf:
            self.use(conf)

    def load_config(self):
        return self.CONFIGS[self.current]

    def get_object(self):
        return self.load_config().__class__

    def use(self, name):
        if name in self.CONFIGS:
            self.current = name

    def __getitem__(self, key):
        conf = self.load_config()
        return getattr(conf, key, None)

    def __getattr__(self, key):
        conf = self.load_config()
        return getattr(conf, key)


config = Config()
