
import os
from config.default import DefaultConfig


def get_env(key, default=None, make_list=False):
    value = os.environ.get(key, default)
    if make_list:
        return value.split(',') if value else []
    return value


class WsgiConfig(DefaultConfig):
    DEBUG = True
    TESTING = False

    PROJECT = get_env('PROJECT')
    MAIN_DOMAIN = get_env('MAIN_DOMAIN')
    DATABASE_URI = os.environ.get('DATABASE_URL')
    BROKER_URL = os.environ.get('RABBITMQ_BIGWIG_URL')
    CELERY_ALWAYS_EAGER = False

    OAUTH_BACKENDS = get_env('OAUTH_BACKENDS', make_list=True)
    OAUTH_USER_CREATION = False

    FACEBOOK_APP_ID = get_env('FACEBOOK_APP_ID')
    FACEBOOK_SECRET = get_env('FACEBOOK_SECRET')

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': '/tmp/wally.log',
                'formatter': 'standard',
            },
            'logentries': {
                'level': 'INFO',
                'class': 'logentries.LogentriesHandler',
                'token': get_env('LOGENTRIES_TOKEN'),
                'formatter': 'standard',
            }
        },
        'loggers': {
            '': {
                'handlers': ['file', 'logentries'],
                'level': 'INFO',
                'propagate': True,
            },
        }
    }
