
import os
from config.default import DefaultConfig


class TestConfig(DefaultConfig):

    TESTING = True
    DATABASE_URI = os.environ.get(
        'DATABASE_URL',
        'postgresql://wally:wally@localhost/wally_test'
    )

    CELERY_ALWAYS_EAGER = True

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'filename': '/tmp/wally_tests.log',
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['file'],
                'level': 'INFO',
                'propagate': True,
            },
        }
    }
