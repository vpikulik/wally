from sqlalchemy import create_engine, Table
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from config import config

Base = declarative_base()


class DB(object):

    def __init__(self):
        self.metadata = Base.metadata

    def start(self):
        self.engine = create_engine(
            config['DATABASE_URI'],
            convert_unicode=True
        )
        self.metadata.bind = self.engine
        self.session = scoped_session(sessionmaker(autocommit=False,
                                                   autoflush=False,
                                                   bind=self.engine))
        Base.query = self.session.query_property()

    def flush(self):
        self.session.flush()

    def commit(self):
        self.session.commit()

    def finish(self):
        self.session.close()

    def init_db(self):
        import core.models
        self.metadata.create_all()

    def drop_db(self):
        self.metadata.drop_all()

    def execute(self, *args, **kwargs):
        self.session.execute(*args, **kwargs)

    def get_view(self, name):
        return Table(name, self.metadata, autoload=True)


db = DB()
