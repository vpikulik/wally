
import urllib
import spidermonkey
from flask import url_for, current_app

from core.database import db
from lib.sqlalchemy_view import SqlViewDrop


def get_config(key, default=None):
    return current_app.config.get(key, default)


def url(*args, **kwargs):
    if 'absolute' in kwargs:
        absolute = kwargs.pop('absolute')
    else:
        absolute = False

    base_url = url_for(*args, **kwargs)
    if absolute:
        return urllib.basejoin(get_config('MAIN_DOMAIN'), base_url)
    else:
        return base_url


def spidermonkey_load(obj):

    if isinstance(obj, spidermonkey.Array):
        return map(
            spidermonkey_load,
            obj
        )
    elif isinstance(obj, spidermonkey.Object):
        return dict(
            map(
                lambda key: (key, spidermonkey_load(getattr(obj, key))),
                obj
            )
        )
    else:
        return obj


def drop_all_views():
    from core.models import ViewDescription

    views = ViewDescription.query.all()
    for view in views:
        drop_view = SqlViewDrop('{}_data'.format(view.name), materialized=True)
        db.execute(drop_view)
    db.commit()
