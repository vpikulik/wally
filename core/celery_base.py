
from celery import Celery

from config import config


class CeleryBase(object):

    def __init__(self):
        self.celery = Celery('wally')

    def start_celery(self):
        self.celery.config_from_object(config)

    @property
    def task(self):
        return self.celery.task


celery = CeleryBase()
