
from functools import wraps
from datetime import datetime

from flask_oauthlib.client import OAuth
from flask import session, redirect
from flask.ext.login import (LoginManager, login_user as flask_login_user,
                             logout_user, login_required)
from sqlalchemy.orm.exc import NoResultFound

from core.database import db
from core.models import User
from core.utils import url, get_config

oauth = OAuth()
login_manager = LoginManager()
facebook_cache = None


def get_facebook():
    global facebook_cache
    if not facebook_cache is None:
        return facebook_cache

    facebook = oauth.remote_app(
        'facebook',
        base_url='https://graph.facebook.com/',
        request_token_url=None,
        access_token_url='/oauth/access_token',
        authorize_url='https://www.facebook.com/dialog/oauth',
        consumer_key=get_config('FACEBOOK_APP_ID'),
        consumer_secret=get_config('FACEBOOK_SECRET'),
        request_token_params={'scope': 'email'}
    )

    @facebook.tokengetter
    def get_facebook_token(token=None):
        return session['facebook_token']
    facebook_cache = facebook
    return facebook


class AuthError(Exception):

    def __init__(self, msg='', status=403):
        self.msg = msg
        self.status = status


def get_provider(provider):
    if not provider in get_config('OAUTH_BACKENDS'):
        raise AuthError('Wrong provider')
    try:
        return {
            'facebook': get_facebook(),
        }[provider]
    except KeyError:
        raise AuthError('Wrong provider')


def facebook_callbackback_data():
    facebook = get_facebook()
    resp = facebook.authorized_response()
    if not (isinstance(resp, dict) and 'access_token' in resp):
        raise AuthError('Wrong Facebook response')

    session['facebook_token'] = (
        resp['access_token'],
        None
    )

    resp = facebook.get('me')
    return {
        'email': resp.data['email'],
        'name': resp.data['name'],
        'projects': ['*'],
        'role': 'admin',
    }


def get_callback_data(provider):
    if not provider in get_config('OAUTH_BACKENDS'):
        raise AuthError('Wrong provider')
    try:
        return {
            'facebook': facebook_callbackback_data,
        }[provider]
    except KeyError:
        raise AuthError('Wrong provider')


def get_user(email, projects, name=None, role=None):

    project = get_config('PROJECT')
    if not ('*' in projects or project in projects):
        raise AuthError('User is not allowed in {}'.format(project))

    try:
        user = User.query.filter(User.email == email).one()
    except NoResultFound:
        if not get_config('OAUTH_USER_CREATION'):
            raise AuthError('User does not exist')
        user = User(email=email)

    if not name is None:
        user.name = name
    if not role is None:
        user.role = role
    user.save()
    db.commit()

    return user


def catch_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except AuthError, exception:
            return exception.msg, exception.status
    return wrapper


@catch_exception
def login(provider):
    return get_provider(provider).authorize(
        callback=url('oauth_authorized', absolute=True, provider=provider)
    )


@login_required
def logout():
    logout_user()
    return 'Logged out'


def login_user(user):
    flask_login_user(user)

    user.login_at = datetime.now()
    user.save()
    db.commit()


@catch_exception
def oauth_authorized(provider):
    data = get_callback_data(provider)()
    user = get_user(**data)
    login_user(user)
    return redirect('/admin/')


@login_manager.user_loader
def load_user(userid):
    try:
        return User.query.filter(User.id == userid).one()
    except NoResultFound:
        return None


def init_auth(app):
    login_manager.init_app(app)
    app.route('/login/<provider>/')(login)
    app.route('/login/<provider>/back/')(oauth_authorized)
    app.route('/logout/')(logout)
