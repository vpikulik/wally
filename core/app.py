
import logging.config
from flask import Flask

from admin import init_admin
from api import init_api
from core.auth import init_auth
from config import config


def create_app():
    app = Flask('wally')
    app.config.from_object(config.get_object())
    app.secret_key = '12345'

    logging.config.dictConfig(config['LOGGING'])

    init_admin(app)
    init_api(app)
    init_auth(app)
    return app
