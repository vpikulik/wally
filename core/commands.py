
import re
import requests
import logging
import spidermonkey
from datetime import datetime, timedelta

from core.database import db
from core.models import Action, Instruction, Profile
from core.utils import spidermonkey_load

logger = logging.getLogger(__name__)


class InvalidCommand(Exception):

    def __init__(self, message):
        super(InvalidCommand, self).__init__()
        self.message = message


class BaseCommand(object):

    def parse(self, raw):
        return {}

    def run(self, action):
        return None


class FakeCommand(BaseCommand):
    pass


class WaitCommand(BaseCommand):

    PARSE_RE = re.compile(r'(?P<value>[0-9]+)(:? )?(?P<nominal>[a-zA-Z]+)')

    TRANSFORM_MAP = {
        'second': 1,
        'seconds': 1,
        'minute': 60,
        'minutes': 60,
        'hour': 60 * 60,
        'hours': 60 * 60,
        'day': 60 * 60 * 24,
        'days': 60 * 60 * 24,

    }

    def parse(self, raw):
        result = self.PARSE_RE.search(raw)
        if not result:
            raise InvalidCommand('Can not parse command')

        groups = result.groupdict()
        value = groups['value']
        nominal = groups['nominal']

        if not nominal in self.TRANSFORM_MAP:
            raise InvalidCommand('Wrong nominal')

        value = int(value) * self.TRANSFORM_MAP[nominal]
        return {'timeout': value}

    def run(self, action):

        try:
            timeout = action.instruction.compiled_params['timeout']
        except (KeyError, TypeError):
            raise Instruction.CancelWithError()

        new_action = Action.create(
            tactic=action.instruction.tactic,
            profile=action.profile,
            previous_instruction=action.instruction,
        )
        if new_action is None:
            logger.info('action="run_wait_command", msg="Last instruction"')
            return

        new_action.start_time = datetime.now() + timedelta(seconds=timeout)
        return new_action


class JsBaseCommand(BaseCommand):

    def __init__(self, *args, **kwargs):
        super(JsBaseCommand, self).__init__(*args, **kwargs)
        self.profile = None
        self.profile_context = None

    def parse(self, raw):
        return raw

    def init_js_context(self):
        rt = spidermonkey.Runtime()
        return rt.new_context()

    def get_js_context(self, params=None):
        context = self.init_js_context()
        if not params is None:
            for key, value in params.items():
                context.add_global(key, value)
        return context

    def _profile_save(self):
        # import ipdb; ipdb.set_trace()
        if self.profile.email != self.profile_context['email']:
            raise Instruction.CancelWithError()

        Profile.query.filter(Profile.email == self.profile.email).update({
            'name': self.profile_context['name'],
            'data': self.profile_context.get('data', {})
        })
        db.commit()

    def run(self, action):

        instruction = action.instruction
        self.profile = action.profile

        if not instruction.params:
            raise Instruction.CancelWithError()

        self.profile_context = self.profile.get_context()
        self.profile_context['save'] = self._profile_save
        context = self.get_js_context(
            params={'profile': self.profile_context}
        )
        try:
            result = context.execute(instruction.params)
        except spidermonkey.JSError:
            raise Instruction.CancelWithError()

        if not result:
            logger.info('action="run_js_command", msg="Negative", '
                        'profile="%s"', self.profile.email)
            return

        new_action = Action.create(
            tactic=instruction.tactic,
            profile=self.profile,
            previous_instruction=instruction,
        )
        if new_action is None:
            logger.info('action="run_js_command", msg="Last instruction", '
                        'profile="%s", instruction="%s"', self.profile.email,
                        instruction)
            return

        return new_action


class JsRequestCommand(JsBaseCommand):

    def headers(self, params=None):
        base = {'ContentType': 'application/json'}
        if params:
            base.update(params)
        return base

    def prepare_response(self, response):
        if response.status_code != 200:
            return False

        try:
            return response.json()
        except ValueError:
            return response.content

    def get(self, url, params=None, headers=None):
        resp = requests.get(
            url,
            params=spidermonkey_load(params),
            headers=self.headers(headers)
        )
        return self.prepare_response(resp)

    def post(self, url, data=None, headers=None):
        resp = requests.post(
            url,
            data=spidermonkey_load(data),
            headers=self.headers(headers)
        )
        return self.prepare_response(resp)

    def put(self, url, data=None, headers=None):
        resp = requests.put(
            url,
            data=spidermonkey_load(data),
            headers=self.headers(headers)
        )
        return self.prepare_response(resp)

    def delete(self, url, headers=None):
        resp = requests.delete(
            url,
            headers=self.headers(headers)
        )
        return self.prepare_response(resp)

    def get_js_context(self, params=None):
        context = super(JsRequestCommand, self).get_js_context(params)
        context.add_global('client', {
            'get': self.get,
            'post': self.post,
            'put': self.put,
            'delete': self.delete,
        })
        return context


library = {
    'FAKE': FakeCommand,
    'WAIT': WaitCommand,
    'JS': JsBaseCommand,
    'JSRequest': JsRequestCommand,
}
