
import logging
from datetime import datetime
from sqlalchemy import null
from sqlalchemy.orm.exc import NoResultFound

from core.celery_base import celery
from core.models import (Event, Profile, ViewDescription, Tactic, Action,
                         ActionDataManager, ProfileDataManager)
from core.database import db
from lib.sqlalchemy_view import SqlViewDrop, SqlView

logger = logging.getLogger(__name__)


STATISTIC_VIEW_NAME = 'profile_data'


@celery.task()
def receive_event(event_name, email):
    try:
        event = Event.query.filter(Event.name == event_name).one()
    except NoResultFound:
        logger.error('action="receive_event", msg="Event was not found", '
                     'event_name="%s"', event_name)
        return

    try:
        profile = Profile.query.filter(Profile.email == email).one()
    except NoResultFound:
        logger.error('action="receive_event", msg="Profile was not found", '
                     'email="%s"', email)
        return

    tactics = Tactic.query.filter(Tactic.event == event).all()
    for tactic in tactics:
        action = Action.create(
            tactic=tactic,
            profile=profile,
        )
        action.run()  # TODO: run in task


@celery.task()
def look_for_actions():
    actions = Action.query.filter(
        (Action.start_time <= datetime.now()) | (Action.start_time == null())
    )
    for action in actions:
        action.run()


@celery.task()
def create_view(owner, view_name):

    if owner == 'profile':
        data_manager = ProfileDataManager
    elif owner == 'action':
        data_manager = ActionDataManager
    else:
        logger.error('action="create_view", msg="wrong owner", '
                     'owner="%s"', owner)
        return

    if view_name:
        try:
            view_description = ViewDescription.query.filter(
                ViewDescription.name == view_name).one()
        except NoResultFound:
            logger.error('action="create_view", msg="view not found", '
                         'view_name="%s"', view_name)
            return

    logger.info('action="create_view", view_name="%s", owner="%s"', view_name,
                owner)
    query = data_manager.view_query(view_description=view_description)

    view_db_name = '{}_data'.format(view_name)
    drop_view = SqlViewDrop(view_db_name, materialized=True)
    create_view = SqlView(view_db_name, query.selectable,
                          materialized=True, with_data=True)

    db.execute(drop_view)
    db.execute(create_view)
    db.commit()
    logger.info('action="create_view_finished", view_name="%s", owner="%s"',
                view_name, owner)
