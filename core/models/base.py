
import logging
logger = logging.getLogger(__name__)

from datetime import datetime

from sqlalchemy import Column, types

from core.database import Base, db


class BaseModel(Base):

    __abstract__ = True

    id = Column(types.Integer(), primary_key=True)

    def __init__(self, **kwargs):
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    @classmethod
    def create(cls, * args, **kwargs):
        obj = cls(*args, **kwargs)
        obj.save()
        return obj

    @classmethod
    def exists(cls, condition):
        return cls.query.filter(condition).value(cls.id)

    def __repr__(self):
        return '<{} {}>'.format(
            self.__class__.__name__,
            getattr(self, 'id', '')
        )

    def save(self):
        db.session.add(self)


class MixinTimes(object):
    created_at = Column(types.DateTime, default=datetime.now)
    modified_at = Column(types.DateTime, default=datetime.now,
                         onupdate=datetime.now)
