
import logging
logger = logging.getLogger(__name__)

from datetime import datetime

from sqlalchemy import Column, types, schema
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import NoResultFound

from core.models.base import MixinTimes, BaseModel
from core.models.profile import Profile


class Event(MixinTimes, BaseModel):

    __tablename__ = 'event'

    name = Column(types.String(250), unique=True)

    def __unicode__(self):
        return self.name


class Tactic(MixinTimes, BaseModel):

    __tablename__ = 'tactic'

    event_id = Column(types.Integer(), schema.ForeignKey(Event.id))
    event = relationship(Event)
    title = Column(types.String(250))

    def __unicode__(self):
        return self.title

    def get_instruction(self, previous=None):
        try:
            query = Instruction.query.filter(Instruction.tactic_id == self.id)
            if previous:
                query = query.filter(Instruction.index > previous.index)
            query = query.order_by(Instruction.index.asc()).limit(1)
            return query.one()
        except NoResultFound:
            return None


class Instruction(MixinTimes, BaseModel):

    __tablename__ = 'command'

    METHODS = ('WAIT', 'JS', 'JSRequest', 'FAKE')

    tactic_id = Column(types.Integer(), schema.ForeignKey(Tactic.id))
    tactic = relationship(Tactic, backref='instructions')
    index = Column(types.Integer())
    method = Column(types.Enum(*METHODS, name='commands'))
    params = Column(types.Text)
    compiled_params = Column(JSON)

    def __unicode__(self):
        return self.method

    def load_command(self):
        from core.commands import library
        Command = library[self.method]
        return Command()

    def run(self, action):
        command = self.load_command()
        return command.run(action=action)

    def save(self):
        command = self.load_command()
        self.compiled_params = command.parse(self.params)

        super(Instruction, self).save()

    def __repr__(self):
        return '{} {}'.format(self.method, self.params)

    class CancelWithSuccess(Exception):
        pass

    class CancelWithError(Exception):
        pass


class Action(BaseModel):

    __tablename__ = 'action'

    instruction_id = Column(types.Integer(), schema.ForeignKey(Instruction.id))
    instruction = relationship(Instruction)
    profile_id = Column(types.Integer(), schema.ForeignKey(Profile.id))
    profile = relationship(Profile)
    start_time = Column(types.DateTime)

    @classmethod
    def create(cls, tactic, profile, previous_instruction=None):
        # TODO: add start_time to args?
        instruction = tactic.get_instruction(previous=previous_instruction)
        if instruction:
            action = cls(
                instruction=instruction,
                profile=profile
            )
            return action

    def run(self):
        try:
            result = self.instruction.run(self)
        except Instruction.CancelWithSuccess:
            logger.info('action="action_cancel_with_success", '
                        'instruction="%s", profile="%s"', self.instruction,
                        self.profile)
        except Instruction.CancelWithError:
            logger.info('action="action_cancel_with_error", '
                        'instruction="%s", profile="%s"', self.instruction,
                        self.profile)
        else:
            if isinstance(result, Action):
                result.process()
            elif result is None:
                new_action = Action.create(
                    tactic=self.instruction.tactic,
                    profile=self.profile,
                    previous_action=self
                )
                new_action.process()

        # run instruction
        # catch for exceptions
         ## if no exceptions and new action is returned
              ### -> check time and run or save action
         ## if no exceptions and null is returned -> produce new action (may be finish)
         ## if cancel exception -> stop
         ## if predfined error -> special signal and cancel
         ## error -> special signal and cancel

    def process(self):
        if self.start_time is None or self.start_time <= datetime.now():
            self.run()
        else:
            self.save()
