
import logging
logger = logging.getLogger(__name__)

from dateutil.parser import parse

from sqlalchemy import (Table, Column, UniqueConstraint, types,
                        schema)
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship, aliased
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import exists

from core.database import db
from core.models.base import MixinTimes, BaseModel


class Profile(MixinTimes, BaseModel):

    __tablename__ = 'profile'

    email = Column(types.String(250))
    name = Column(types.String(250))
    data = Column(JSON)

    def __unicode__(self):
        return self.email

    def get_context(self):
        return {
            'email': self.email,
            'name': self.name,
            'data': self.data,
        }

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)
        if self.data and isinstance(self.data, dict):
            self.save_data()

    def save_data(self):
        for key, value in self.data.iteritems():
            try:
                ProfileDataManager.save_data(self, key, value)
            except DataBase.CleanError:
                logger.error('action="profile_save", '
                             'msg="data can not be saved", '
                             'profile_id="%s", key="%s", value="%s"',
                             self.id, key, value)


class Action(MixinTimes, BaseModel):

    __tablename__ = 'profile_action'

    profile_id = Column(types.Integer(), schema.ForeignKey(Profile.id))
    profile = relationship(Profile)
    action_type = Column(types.String(250), nullable=True, index=True)
    data = Column(JSON)

    def save(self, *args, **kwargs):
        super(Action, self).save(*args, **kwargs)
        if self.data and isinstance(self.data, dict):
            self.save_data()

    def save_data(self):
        for key, value in self.data.iteritems():
            try:
                ActionDataManager.save_data(self, key, value)
            except DataBase.CleanError:
                logger.error('action="profile_save", '
                             'msg="data can not be saved", '
                             'profile_id="%s", key="%s", value="%s"',
                             self.id, key, value)


class Field(MixinTimes, BaseModel):

    __tablename__ = 'fields'

    TYPES = ('string', 'integer', 'float', 'boolean', 'datetime')

    name = Column(types.String(250))
    owner = Column(types.Enum('profile', 'action', name='owner_types'),
                   nullable=True)
    ftype = Column(types.Enum(*TYPES, name='field_types'), nullable=True)

    _field_cash = {}

    def __unicode__(self):
        return u'{}<{},{}>'.format(self.name, self.ftype, self.owner)

    def save(self, *args, **kwargs):
        super(Field, self).save(*args, **kwargs)
        Field._field_cash[self.name] = self

    @classmethod
    def get_field(cls, owner, name):
        key = '{}_{}'.format(owner, name)
        if cls._field_cash.get(key):
            return cls._field_cash[key]

        try:
            field = Field.query.filter(Field.owner == owner,
                                       Field.name == name).one()
        except NoResultFound:
            return
        else:
            cls._field_cash[key] = field
            return field


class ProfileMixin(object):

    __owner__ = 'profile'

    @declared_attr
    def profile_id(self):
        return Column(types.Integer(), schema.ForeignKey(Profile.id))

    @declared_attr
    def owner(self):
        return relationship(Profile)


class ActionMixin(object):

    __owner__ = 'action'

    @declared_attr
    def action_id(self):
        return Column(types.Integer(), schema.ForeignKey(Action.id))

    @declared_attr
    def owner(self):
        return relationship(Action)


class DataBase(BaseModel):

    __abstract__ = True

    @declared_attr
    def field_id(self):
        return Column(types.Integer(), schema.ForeignKey(Field.id))

    @declared_attr
    def field(self):
        return relationship(Field)

    @declared_attr
    def __table_args__(self):
        return (
            UniqueConstraint(
                '{}_id'.format(self.__owner__),
                'field_id',
                name='{}_data_{}_unique'.format(self.__owner__,
                                                self.__data_name__)
            ),
        )

    @declared_attr
    def __tablename__(self):
        return '{}_data_{}'.format(self.__owner__, self.__data_name__)

    @classmethod
    def update(cls, owner, field, data):

        cleaned_data = cls.clean(data)
        qs_filter = (cls.owner == owner) & (cls.field == field)
        obj_exists = db.session.query(exists().where(qs_filter)).scalar()

        if obj_exists:
            db.session.query(cls).filter(qs_filter).update(
                {'data': cleaned_data})
        else:
            cls.create(
                owner=owner,
                field=field,
                data=cleaned_data
            )

    class CleanError(Exception):
        pass


class DataString(DataBase):
    __abstract__ = True
    __data_name__ = 'string'

    @declared_attr
    def data(self):
        return Column(types.Text())

    @classmethod
    def clean(cls, value):
        return unicode(value.decode('utf-8'))

    @classmethod
    def column(cls, name):
        return Column(name, types.Text(), nullable=True)


class ProfileDataString(ProfileMixin, DataString):
    pass


class ActionDataString(ActionMixin, DataString):
    pass


class DataInteger(DataBase):
    __abstract__ = True
    __data_name__ = 'integer'

    @declared_attr
    def data(self):
        return Column(types.Integer())

    @classmethod
    def clean(cls, value):
        try:
            return int(value)
        except ValueError:
            raise cls.CleanError()

    @classmethod
    def column(cls, name):
        return Column(name, types.Integer(), nullable=True)


class ProfileDataInteger(ProfileMixin, DataInteger):
    pass


class ActionDataInteger(ActionMixin, DataInteger):
    pass


class DataFloat(DataBase):
    __abstract__ = True
    __data_name__ = 'float'

    @declared_attr
    def data(self):
        return Column(types.Float(precision=2))

    @classmethod
    def clean(cls, value):
        try:
            return float(value)
        except ValueError:
            raise cls.CleanError()

    @classmethod
    def column(cls, name):
        return Column(name, types.Float(precision=2), nullable=True)


class ProfileDataFloat(ProfileMixin, DataFloat):
    pass


class ActionDataFloat(ActionMixin, DataFloat):
    pass


class DataBoolean(DataBase):
    __abstract__ = True
    __data_name__ = 'boolean'

    @declared_attr
    def data(self):
        return Column(types.Boolean())

    @classmethod
    def clean(cls, value):
        if value.lower() in ('true', 't', '1'):
            return True
        if value.lower() in ('false', 'f', '0'):
            return False
        raise cls.CleanError()

    @classmethod
    def column(cls, name):
        return Column(name, types.Boolean(), nullable=True)


class ProfileDataBoolean(ProfileMixin, DataBoolean):
    pass


class ActionDataBoolean(ActionMixin, DataBoolean):
    pass


class DataDatetime(DataBase):
    __abstract__ = True
    __data_name__ = 'datetime'

    @declared_attr
    def data(self):
        return Column(types.DateTime)

    @classmethod
    def clean(cls, value):
        try:
            return parse(value)
        except TypeError:
            raise cls.CleanError()

    @classmethod
    def column(cls, name):
        return Column(name, types.DateTime(), nullable=True)


class ProfileDataDatetime(ProfileMixin, DataDatetime):
    pass


class ActionDataDatetime(ActionMixin, DataDatetime):
    pass


class DataManager(object):

    @classmethod
    def save_data(cls, owner, field_name, data):
        field = Field.get_field(cls.__owner__, field_name)
        if field:
            data_class = cls.MAP[field.ftype]
            data_class.update(owner, field, data)
        else:
            logger.error('action="%s_save_data", '
                         'msg="field is not found", %s="%s", '
                         'field="%s", data="%s"', cls.__owner__,
                         cls.__owner__, owner.id, field_name, data)

    @classmethod
    def view_query(cls, view_description):
        if view_description and view_description.owner != cls.__owner__:
            logger.error('action="view_query", message="View has wrong owner"')
            return

        aliases = {}
        values = cls.QUERY_BASE_COLUMNS[:]
        for field in view_description.fields:
            field_class = cls.MAP[field.ftype]
            aliased_tbl = aliased(
                field_class,
                name='field_{}'.format(field.name)
            )
            aliases[field.id] = aliased_tbl
            values.append(aliased_tbl.data.label(field.name))

        qs = db.session.query(*values)
        for field_id, alias in aliases.iteritems():
            qs = qs.outerjoin(
                alias,
                (
                    (alias.field_id == field_id) &
                    (getattr(alias, cls.__owner_field_name__) ==
                        cls.__model__.id)
                )
            )
        return qs

    @classmethod
    def view_table(cls, view_description):
        if view_description and view_description.owner != cls.__owner__:
            logger.error('action="view_table", message="View has wrong owner"')
            return

        columns = cls.TABLE_BASE_COLUMNS[:]
        for field in view_description.fields:
            field_class = cls.MAP[field.ftype]
            columns.append(field_class.column(field.name))

        view_db_name = '{}_data'.format(view_description.name)
        return Table(
            view_db_name,
            db.metadata,
            *columns
        )


class ProfileDataManager(DataManager):

    __owner__ = 'profile'
    __owner_field_name__ = 'profile_id'
    __model__ = Profile
    QUERY_BASE_COLUMNS = [Profile.name.label('name'),
                          Profile.email.label('email')]
    TABLE_BASE_COLUMNS = [Column('email', types.String(255), nullable=True),
                          Column('name', types.String(255), nullable=True)]

    MAP = {
        'string': ProfileDataString,
        'integer': ProfileDataInteger,
        'float': ProfileDataFloat,
        'boolean': ProfileDataBoolean,
        'datetime': ProfileDataDatetime,
    }


class ActionDataManager(DataManager):

    __owner__ = 'action'
    __owner_field_name__ = 'action_id'
    __model__ = Action
    QUERY_BASE_COLUMNS = [Action.profile_id.label('profile_id')]
    TABLE_BASE_COLUMNS = [
        Column('profile_id', types.Integer(), nullable=True)]

    MAP = {
        'string': ActionDataString,
        'integer': ActionDataInteger,
        'float': ActionDataFloat,
        'boolean': ActionDataBoolean,
        'datetime': ActionDataDatetime,
    }
