
from sqlalchemy import (Table, Column, ForeignKey, types)

from sqlalchemy.orm import relationship

from core.database import Base
from core.models.base import BaseModel, MixinTimes
from core.models.profile import Field


views_vs_fields = Table(
    'views_vs_fields', Base.metadata,
    Column('view_id', types.Integer, ForeignKey('view.id')),
    Column('field_id', types.Integer, ForeignKey('fields.id'))
)


class ViewDescription(MixinTimes, BaseModel):

    __tablename__ = 'view'

    name = Column(types.String(250))
    owner = Column(types.Enum('profile', 'action', name='view_owner_types'),
                   nullable=True)

    fields = relationship(Field, secondary=views_vs_fields, backref='views')
    filter = Column(types.Text())

    def __unicode__(self):
        return self.name


class Report(MixinTimes, BaseModel):

    __tablename__ = 'report'

    name = Column(types.String(250))
    query_code = Column(types.Text())
    query_sql = Column(types.Text(), nullable=True)

    def __unicode__(self):
        return self.name
