
import logging
logger = logging.getLogger(__name__)

from core.models.base import MixinTimes, BaseModel

from sqlalchemy import Column, types


class User(MixinTimes, BaseModel):

    __tablename__ = 'user'

    ROLES = ('admin', 'user',)

    email = Column(types.String(250), unique=True)
    name = Column(types.String(250), nullable=True)
    api_key = Column(types.String(250), unique=True, nullable=True)
    role = Column(types.Enum(*ROLES, name='roles'), default='user')
    login_at = Column(types.DateTime, nullable=True)

    def __unicode__(self):
        return self.email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id
